package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"time"

	gin "github.com/gin-gonic/gin"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/readpref"
)

var db = databaseClient{}

const (
	MSG_ERROR_JSON                string = "Incorrect JSON payload"
	MSG_ERROR_BSON                string = "Incorrect BSON formatting"
	MSG_ERROR_INSERT_DB           string = "Unable to insert in database"
	MSG_ERROR_FIND_DB             string = "Unable to find item in database"
	MSG_ERROR_RETRIEVE_DB         string = "Error retrieving data from DB"
	MSG_ERROR_SERIALIZE_DB_RESULT string = "Error serializing DB results"
)

const (
	UUID_VAR    string = "uuid"
	BASE_URL    string = "/v1/people"
	GET_URL     string = BASE_URL + "/:" + UUID_VAR
	GET_ALL_URL string = BASE_URL
	POST_URL    string = BASE_URL
	DELETE_URL  string = BASE_URL + "/:" + UUID_VAR
	PUT_URL     string = BASE_URL + "/:" + UUID_VAR
)

const (
	defaultTimeout time.Duration = 10 * time.Second
	mongoDatabase  string        = "api-exercise"
)

var (
	mongoHost       *string
	mongoCollection *string
)

type databaseClient struct {
	Client     *mongo.Client
	Collection *mongo.Collection
}

type entity struct {
	UUID                    primitive.ObjectID `json:"uuid" bson:"_id" binding:"omitempty"`
	Survived                string             `json:"survived" bson:"survived" binding:"required"`
	PassengerClass          int64              `json:"passengerClass" bson:"passengerClass" binding:"omitempty"`
	Name                    string             `json:"name" bson:"name" binding:"required"`
	Sex                     string             `json:"sex" bson:"sex" binding:"required"`
	Age                     float64            `json:"age" bson:"age" binding:"required"`
	SiblingsOrSpousesAboard int64              `json:"siblingsOrSpousesAboard" bson:"siblingsOrSpousesAboard" binding:"omitempty"`
	ParentsOrChildrenAboard int64              `json:"parentsOrChildrenAboard" bson:"parentsOrChildrenAboard" binding:"omitempty"`
	Fare                    float64            `json:"fare" bson:"fare" binding:"required"`
}

func (dbClient *databaseClient) Connect() error {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	dbClient.Client, _ = mongo.Connect(ctx, "mongodb://admin:admin@"+*mongoHost+"/?replicaSet=MainRepSet&authSource="+mongoDatabase)
	err := dbClient.Client.Ping(ctx, readpref.Primary())
	return err
}

func (dbClient *databaseClient) Init() {
	dbClient.Collection = dbClient.Client.Database(mongoDatabase).Collection(*mongoCollection)
}

func (e entity) getHandle(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	objID, err := primitive.ObjectIDFromHex(c.Param(UUID_VAR))
	if err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"status": "KO", "msg": MSG_ERROR_BSON})
		return
	}

	filter := bson.M{"_id": objID}
	if err := db.Collection.FindOne(ctx, filter).Decode(&e); err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusOK, gin.H{"status": "KO", "msg": MSG_ERROR_FIND_DB})
		return
	}

	c.JSON(http.StatusOK, e)
}

func (e entity) getAllHandle(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	filter := bson.M{}
	cursor, err := db.Collection.Find(ctx, filter)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"status": "KO", "msg": MSG_ERROR_RETRIEVE_DB})
		return
	}

	response := make([]entity, 0)
	for cursor.Next(ctx) {
		var entityReponse entity
		err := cursor.Decode(&entityReponse)
		if err != nil {
			log.Printf("Error: %s", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"status": "KO", "msg": MSG_ERROR_SERIALIZE_DB_RESULT})
			return
		}
		response = append(response, entityReponse)
	}

	c.JSON(http.StatusOK, response)
}

func (e entity) postHandle(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	if err := c.ShouldBindJSON(&e); err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"status": "KO", "msg": MSG_ERROR_JSON})
		return
	}
	e.UUID = primitive.NewObjectID()

	_, err := db.Collection.InsertOne(ctx, e)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"status": "KO", "msg": MSG_ERROR_INSERT_DB})
		return
	}

	c.JSON(http.StatusOK, e)
}

func (e entity) deleteHandle(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	objID, err := primitive.ObjectIDFromHex(c.Param(UUID_VAR))
	if err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"status": "KO", "msg": MSG_ERROR_BSON})
		return
	}

	filter := bson.M{"_id": objID}
	db.Collection.FindOneAndDelete(ctx, filter)

	c.JSON(http.StatusOK, gin.H{})
}

func (e entity) putHandle(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	if err := c.ShouldBindJSON(&e); err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"status": "KO", "msg": MSG_ERROR_JSON})
		return
	}

	objID, err := primitive.ObjectIDFromHex(c.Param(UUID_VAR))
	if err != nil {
		log.Printf("Error: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"status": "KO", "msg": MSG_ERROR_BSON})
		return
	}
	e.UUID = objID

	filter := bson.M{"_id": objID}
	db.Collection.FindOneAndUpdate(ctx, filter, bson.M{"$set": e})

	c.JSON(http.StatusOK, gin.H{})
}

func init() {
	mongoHost = flag.String("database", "mongod-0.mongodb-service.default.svc.cluster.local:27017,mongod-1.mongodb-service.default.svc.cluster.local:27017", "a comma separated string of database DNS's and ports to target")
	mongoCollection = flag.String("collection", "titanic", "a string representing the Mongo collection to target")
	flag.Parse()

	if err := db.Connect(); err != nil {
		panic(err.Error())
	}
	db.Init()
}

func serve() {
	router := gin.Default()

	var entity entity

	router.GET(GET_URL, entity.getHandle)
	router.GET(GET_ALL_URL, entity.getAllHandle)
	router.POST(POST_URL, entity.postHandle)
	router.DELETE(DELETE_URL, entity.deleteHandle)
	router.PUT(PUT_URL, entity.putHandle)
	router.Run(":8080")
}

func main() {
	serve()
}
