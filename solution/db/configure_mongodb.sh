#!/bin/bash

set -e

# Move titanic file to mounted persistent volume location so that it's accessible from the pod
sudo cp titanic.csv /mnt/data/0/

# Initiate replica set configuration
echo "Configuring the MongoDB Replica Set"
kubectl exec mongod-0 -c mongod-container -- mongo --eval 'rs.initiate({_id: "MainRepSet", version: 1, members: [ {_id: 0, host: "mongod-0.mongodb-service.default.svc.cluster.local:27017"},{_id: 1, host: "mongod-1.mongodb-service.default.svc.cluster.local:27017"} ]});'

# Wait a bit until the replica set should have a primary ready
echo "Waiting for the Replica Set to initialise..."
sleep 30
kubectl exec mongod-0 -c mongod-container -- mongo --eval 'rs.status();'

# Create the admin user (this will automatically disable the localhost exception)
echo "Creating user: 'admin'"
kubectl exec mongod-0 -c mongod-container -- mongo --eval 'db.getSiblingDB("api-exercise").createUser({user:"admin",pwd:"admin",roles:[{role:"root",db:"admin"}]});'
kubectl exec mongod-0 -c mongod-container -- mongo --eval 'db.getSiblingDB("api-exercise").createCollection("titanic");'
kubectl exec mongod-0 -c mongod-container -- mongoimport -d api-exercise -c titanic --type csv --file /data/db/titanic.csv --headerline
