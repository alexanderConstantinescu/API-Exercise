# Container solution challenge 

# Solution

The directory is structured as follows:

```
.
├── api
│   └── api.go
├── db
│   ├── configure_mongodb.sh
│   └── titanic.csv
├── deployments
│   ├── deployment_api.yaml
│   └── deployment_db.yaml
├── Dockerfile
└── README.md

```

The solution implements a simple HTTP API according to the requirements defined in `../API.md`. The solution uses a MongoDB to store the data with a MongoDB ReplicaSet configuration, this is done as to provide a highly available configuration for the deployment of this API (MongoDB also provides the feature of Sharding the DB on top of using multiple replicas - this has not been done during this exercise however). As to deploy the application and database, you have two files in the sub directory `deployments`. `deployment_db.yaml` provides the k8s deployment for the MongoDB ReplicaSet: persistentvolumes, persistentvolumeclaims, statefulset and service. `deployment_api.yaml` provides the deployment for the api. 

The DB and API have a pre-defined scaling factor set to 2. Moreover, the statefulset means that even if we loose all MongoDB pods, once they are re-booted, the latest state of the data is restored. There are some things that need to be taken into account if scaling the MongoDB:

- You will need to create additional persistentvolumes. Currently only two are created (one for each pod replica)
- [NOT Required] You can pass an argument to the API in its deployment template specifying the additional headless services which are created when scaling the Mongo database. Headless services are ordered and useful for statefulsets as they keep a "state" surrounding their qualified DNS, for example: mongod-0.mongodb-service.default.svc.cluster.local, mongod-1.mongodb-service.default.svc.cluster.local, etc. The API binary has two optional flags which can be specified, and one of them is the mongo DB headless services which should be targetted. Please specify this a comma separated string. An example of this in the `deployment_api.yaml` is as follows (for two MongoDB pod replica). This allows MongoDB go driver to balance the work on the ReplicaSet, this is also the reason why it is not necessary but beneficial, as the data will be replicated on all instances of the MongoDB, but the connection from the api to the DB is not load balanced.

For ex in the `deployments/deployment_api.yaml`

```
      containers:
      - image: container-solution:latest
        args: ["-database", "mongod-0.mongodb-service.default.svc.cluster.local:27017,mongod-1.mongodb-service.default.svc.cluster.local:27017"]
```

You can also create a new collection in the database and provide that one as a target using the args to the binary again. The reason for this is as to be able to allow different collections per environment (DEV, TEST, STG, PRD, etc). As such you can separate the testing per environment and target each collecton individually. 

The `Dockerfile` use a two stage build used to build a small image of the API (~ 21MB). This image is then referenced in the `deployment_api.yaml`. 

The API uses the offcial mongo-db-go-driver to be able to communicate with the MongoDB. 

# How to 

How to deploy the application?

1) Start by building the image locally 

```
docker build . -t container-solution:latest
```

2) Now that you have the image you can start deploying the DB

```
kubectl create -f deployments/deployment_db.yaml
```

This creates all necessary resources for the database to deploy

3) Configure the database

```
cd db && ./configure_mongodb.sh
```

This script configures MongoDB inside the pods and initializes the replica set, user, credentials and imports the titanic data.

4) Deploy the API

```
kubectl create -f deployments/deployment_api.yaml
```

This deploys two replicas of the API using the image built in step 1) and one service used to load balance between the pods. If you need to scale it just edit the k8s deployment object.

5) Test the API using the service created

```
curl -X GET ${API_SERVICE_IP}:8080/v1/people
```

# GOTCHAS

There is only one gotcha with respect to the initial requirements on the API

**The JSON field "survived" needs to be wrapped as a string of "true"/"false", i.e not true/false**

The reason for this is that MongoDB does not allow for boolean imports seemlessly without modifying the structure of the collection to reference the type of the field represented (something which I did not have the time to do). I thus left the titanic data "as is" but with the drawback of not having the field "survived" represented as a boolean (only semantically of course). 

Thus to test the API with appended data please perform the following for example:

```
curl -X POST ${API_SERVICE_IP}:8080/v1/people -d '{ "survived": "true", "passengerClass": 3, "name": "Alex", "sex": "male", "age": 22, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":10, "fare":7.2878785}'
```

# TODO

With more time assigned to this task I would have done the following:

* Changed the structure of the Mongo collection to reference the fields type
* Added unit tests for the API


